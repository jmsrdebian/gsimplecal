gsimplecal (2.1-2) unstable; urgency=medium

  * QA upload.
  * Run wrap-and-sort.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper >= 9' to
        'debhelper-compat = 13' in Build-Depends field.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Changed the priority from extra to optional.
      - Removed no longer needed build dependency on dh_autoreconf.
      - Updated the Vcs-* fields and URLs to salsa.debian.org.
      - Use https protocol in 'Homepage' field.
  * debian/copyright:
      - Use https protocol in Format field.
      - Updated upstream and packaging copyright years.
  * debian/rules: removed '--with autoreconf' because it is default
      in the DH 10.
  * debian/salsa-ci.yml: added to provide CI tests for salsa.
  * debian/tests/control: created to provide a basic CI test.
  * debian/upstream/metadata: created.

 -- Jair Reis <jmsrdebian@protonmail.com>  Fri, 12 Jun 2020 13:41:56 -0300

gsimplecal (2.1-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * debian/rules:
      - Include DEB_BUILD_MAINT_OPTIONS for hardening.
      - Removed some comments.
  * debian/control:
      - Bumped Standards-Version to 3.9.6.
      - Removed some extra spaces and character /
  * Add debian/lintian-overrides for false positive
    about hardening-no-fortify-functions.
  * debian/copyright: organize order of files and licenses.
  * Improve debian/watch.

 -- Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>  Wed, 29 Jul 2015 09:07:00 -0300

gsimplecal (2.0-1) unstable; urgency=medium

  * QA upload.
  * Update maintainer to Debian QA Group.
  * Imported Upstream version 2.0
  * d/copyright: Update copyright years.
  * d/control, d/compat: Update to debhelper compat 9.
  * d/control, d/rules: Drop use of hardening-wrapper,
    enable all hardening options.
  * d/watch: Move away from the github redirect, no longer needed.
  * Update to standards-version 3.9.5.
  * d/control: Update build-dep and description for GTK3.
  * d/control: Update VCS links to use collab-maint.

 -- Unit 193 <unit193@ubuntu.com>  Sat, 26 Jul 2014 13:21:40 -0400

gsimplecal (1.6-1) unstable; urgency=low

  * New upstream release:
    + Add option to force LANG for changing weekdays order
  * Make use of hardening-wrapper to enable hardening features

 -- Julien Valroff <julien@debian.org>  Sat, 14 Jul 2012 10:14:36 +0200

gsimplecal (1.5-1) unstable; urgency=low

  * New upstream release
  * Remove patch applied upstream

 -- Julien Valroff <julien@debian.org>  Fri, 06 Apr 2012 15:59:48 +0200

gsimplecal (1.4-1) unstable; urgency=low

  * New upstream release
  * Update sample config file for new options

 -- Julien Valroff <julien@debian.org>  Fri, 02 Mar 2012 19:44:05 +0100

gsimplecal (1.2-1) unstable; urgency=low

  * New upstream release
  * Switch to dh-autoreconf as upstream tarball does not ship configure script
    anymore
  * Add patch to fix manpage hyphen-used-as-minus-sign errors
  * Update DEP-5 format URI
  * Update to latest policy 3.9.3

 -- Julien Valroff <julien@debian.org>  Fri, 02 Mar 2012 19:00:11 +0100

gsimplecal (1.1-1) unstable; urgency=low

  * New upstream release
  * Use versioned DEP-5 uri
  * Update sample config file for new options

 -- Julien Valroff <julien@debian.org>  Sat, 01 Oct 2011 13:16:22 +0200

gsimplecal (1.0-1) unstable; urgency=low

  * New upstream release:
    + Fix bug with external viewer when mark_today is false
    + Add ability to show week numbers — show_week_numbers option
  * Remove patch merged upstream
  * Fix typo in debian/config.sample (thanks to Dmitry Medvinsky
    <dmedvinsky@gmail.com> for spotting this)
  * Add new show_week_number option to configuration sample

 -- Julien Valroff <julien@debian.org>  Mon, 05 Sep 2011 15:50:15 +0200

gsimplecal (0.9-1) unstable; urgency=low

  * Initial release (Closes: #633342)

 -- Julien Valroff <julien@debian.org>  Thu, 01 Sep 2011 06:51:37 +0200
